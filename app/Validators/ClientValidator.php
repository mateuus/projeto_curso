<?php
/**
 * Created by PhpStorm.
 * User: mateus
 * Date: 15/07/16
 * Time: 02:03
 */

namespace CodeProject\Validators;


use Prettus\Validator\LaravelValidator;

class ClientValidator extends LaravelValidator
{

    /**
     * @var array
     */
    protected $rules = [
        'name' => 'required|max:255',
        'responsible' => 'required|max:255',
        'email' => 'required|email',
        'phone' => 'required',
        'address' => 'required',
    ];

}