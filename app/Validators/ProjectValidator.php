<?php
/**
 * Created by PhpStorm.
 * User: mateus
 * Date: 15/07/16
 * Time: 02:03
 */

namespace CodeProject\Validators;


use Prettus\Validator\LaravelValidator;

class ProjectValidator extends LaravelValidator
{

    /**
     * @var array
     */
    protected $rules = [
        'owner_id' => 'required|exists:users,id', 
        'client_id' => 'required|exists:clients,id', 
        'name' => 'required|max:255',
        'description' => 'required',
        'progress' => 'required',
        'status' => 'required',
        'due_date' => 'required',
    ];
    
}