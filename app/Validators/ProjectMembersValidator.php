<?php

namespace CodeProject\Validators;

use \Prettus\Validator\LaravelValidator;

/**
 * Class ProjectMembersValidator.
 *
 * @package namespace CodeProject\Validators;
 */
class ProjectMembersValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        //'project_id' => 'required|exists:projects,id',
        'user_id' => 'required|exists:users,id'
    ];
}
