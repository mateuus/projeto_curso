<?php

namespace CodeProject\Validators;

use \Prettus\Validator\LaravelValidator;

class ProjectTaskValidator extends LaravelValidator {

    protected $rules = [
        'project_id' => 'required|exists:projects,id',
        'name' => 'required|max:255',
        'status' => 'required|integer',
        'due_date' => 'required',
        'start_date' => 'required',
    ];
}
