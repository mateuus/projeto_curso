<?php
/**
 * Created by PhpStorm.
 * User: mateus
 * Date: 15/07/16
 * Time: 02:03
 */

namespace CodeProject\Validators;


use Prettus\Validator\LaravelValidator;

class ProjectNoteValidator extends LaravelValidator
{

    /**
     * @var array
     */
    protected $rules = [
        'project_id' => 'required|exists:projects,id',
        'title' => 'required|max:255',
        'note' => 'required',
    ];
    
}