<?php
/**
 * Created by PhpStorm.
 * User: mateus
 * Date: 15/07/16
 * Time: 01:50
 */

namespace CodeProject\Services;


use CodeProject\Entities\ProjectMembers;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Validators\ProjectMembersValidator;
use CodeProject\Validators\ProjectValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectService {

    /**
     * @var ProjectRepository
     */
    protected $repository;

    /**
     * @var ProjectValidator
     */
    protected $validator;

    /**
     * @var ProjectMembersValidator
     */
    protected $validatorMember;

    /**
     * ProjectService constructor.
     * @param ProjectRepository $repository
     * @param ProjectValidator $validator
     */
    public function __construct(ProjectRepository $repository, ProjectValidator $validator, ProjectMembersValidator $validatorMember) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->validatorMember = $validatorMember;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();

            return $this->repository->create($data);

        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();

            return $this->repository->update($data, $id);

        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }

    }

    /**
     * @param array $data
     * @param $id
     * @return array
     */
    public function addMember(array $data, $id) {
        try {
            $this->validatorMember->with($data)->passesOrFail();

            $members = new ProjectMembers();
            $members->user_id = $data['user_id'];
            return $this->repository->find($id)->members()->save($members);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param $id_project
     * @param $id_user
     * @return mixed
     */
    public function removeMember($id_project, $id_user) {
        return $this->repository->find($id_project)->members()->where('user_id', $id_user)->delete();
    }

    /**
     * @param $id_project
     * @param $id_user
     * @return mixed
     */
    public function isMember($id_project, $id_user) {
        return $this->repository->find($id_project)->members()->where('user_id', $id_user)->get();
    }
}