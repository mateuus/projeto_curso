<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('oauth/access_token', function () {
    return Response::json(\LucaDegasperi\OAuth2Server\Facades\Authorizer::issueAccessToken());
});

Route::group(['middleware' => 'oauth'], function () {

    Route::resource('client', 'ClientController', ['except' => ['create', 'edit']]);

    Route::resource('project', 'ProjectController', ['except' => ['create', 'edit']]);
    Route::group(['prefix' => 'project'], function () {

        // **** PROJECT MEMBERS ********************************************************
        Route::get('{id_project}/members', 'ProjectController@getMembers');
        Route::post('{id_project}/addMember', 'ProjectController@addMember');
        Route::get('{id_project}/isMember/{id_user}', 'ProjectController@isMember');
        Route::delete('{id_project}/removeMember/{id_user}', 'ProjectController@removeMember');

        Route::resource('{id}/note', 'ProjectNoteController', ['except' => ['create', 'edit']]);
        Route::resource('{id}/task', 'ProjectTasksController', ['except' => ['create', 'edit']]);
    });

});