<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Repositories\ProjectRepository;
use CodeProject\Services\ProjectService;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class ProjectController extends Controller {

    /**
     *
     * @var ProjectRepository
     */
    private $repository;

    /**
     * @var ProjectService
     */
    private $service;

    /**
     * ProjectController constructor.
     * @param ProjectRepository $repository
     * @param ProjectService $service
     */
    public function __construct(ProjectRepository $repository, ProjectService $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index() {
        return $this->repository->with(['user', 'client'])->findWhere(['owner_id' => Authorizer::getResourceOwnerId()]);
    }

    /**
     * @param Request $request
     * @return static
     */
    public function store(Request $request) {
        return $this->service->create($request->all());
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id) {
        try {

            if (!$this->checkProjectOwner($id)) {
                return response(['error' => 'Access Forbidden']);
            }

            return $this->repository->with(['user', 'client', 'tasks', 'members', 'notes'])->find($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Projeto não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao exibir o projeto.' . $e->getMessage()]);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($id, Request $request) {
        try {
            if (!$this->checkProjectOwner($id)) {
                return response(['error' => 'Access Forbidden']);
            }
            return $this->service->update($request->all(), $id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Projeto não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao atualizar o projeto.']);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id) {
        try {
            if (!$this->checkProjectOwner($id)) {
                return response(['error' => 'Access Forbidden']);
            }
            if ($this->repository->delete($id)) {
                return response(['msg' => 'Projeto deletado com sucesso!']);
            } else {
                return response(['msg' => 'Erro ao deletar!']);
            }
            return $this->repository->find($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Projeto não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao deletar o projeto.']);
        }
    }

    /**]
     * @param $id_project
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|void
     */
    public function addMember($id_project, Request $request) {

        try {
            return $this->service->addMember($request->all(), $id_project);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Projeto ou Usuário não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao adicionar o membro.']);
        }
    }

    /**
     * @param $id_project
     * @param $id_user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|void
     */
    public function removeMember($id_project, $id_user) {

        try {
            if ($this->service->removeMember($id_project, $id_user)) {
                return response(['msg' => 'Membro deletado com sucesso!']);
            }
            return response(['msg' => 'Erro ao deletar!']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Projeto ou Usuário não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao adicionar o membro.']);
        }
    }

    /**
     * @param $id_project
     * @param $id_user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function isMember($id_project, $id_user) {
        try {
            return $this->service->isMember($id_project, $id_user);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Projeto ou Usuário não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao exibir o projeto.']);
        }
    }

    /**
     * @param $id_project
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getMembers($id_project) {
        try {
            return $this->repository->find($id_project)->members()->get();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Projeto ou Usuário não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao exibir os membros.' . $e->getMessage()]);
        }
    }


    /**
     * @param $projectId
     * @return mixed
     */
    private function checkProjectOwner($projectId) {
        return $this->repository->isOwner($projectId, Authorizer::getResourceOwnerId());
    }
}
