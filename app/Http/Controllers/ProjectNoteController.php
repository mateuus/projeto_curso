<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Repositories\ProjectNoteRepository;
use CodeProject\Services\ProjectNoteService;
use Illuminate\Http\Request;

class ProjectNoteController extends Controller {

    /**
     *
     * @var ProjectRepository
     */
    private $repository;

    /**
     * @var ProjectService
     */
    private $service;

    /**
     * ProjectController constructor.
     * @param ProjectRepository $repository
     * @param ProjectService $service
     */
    public function __construct(ProjectNoteRepository $repository, ProjectNoteService $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index($id) {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * @param Request $request
     * @return static
     */
    public function store(Request $request) {
        return $this->service->create($request->all());
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id, $noteId) {
        try {
            return $this->repository->findWhere(['project_id' => $id, 'id' => $noteId]);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Notacao do projeto não encontrada.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao exibir a notacao do projeto.']);
        }
    }

    /**
     *
     * @param type $id
     * @param type $noteId
     * @param Request $request
     * @return type
     */
    public function update($id, $noteId, Request $request) {
        try {
            return $this->service->update($request->all(), $noteId);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Notacao não encontrada.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao atualizar a notacao.']);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id, $noteId) {
        try {
            if ($this->repository->delete($noteId)) {
                return response(['msg' => 'Notacao deletada com sucesso!']);
            } else {
                return response(['msg' => 'Erro ao deletar!']);
            }
            return $this->repository->find($noteId);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Notacao não encontrada.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao deletar a notacao.']);
        }
    }

}
