<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;

use CodeProject\Repositories\ProjectTaskRepository;
use CodeProject\Validators\ProjectTaskValidator;


class ProjectTasksController extends Controller {

    /**
     * @var ProjectTaskRepository
     */
    protected $repository;

    /**
     * @var ProjectTaskValidator
     */
    protected $validator;

    public function __construct(ProjectTaskRepository $repository, ProjectTaskValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index($id) {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * @param Request $request
     * @return static
     */
    public function store(Request $request) {
        return $this->service->create($request->all());
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id, $taskId) {
        try {
            return $this->repository->findWhere(['project_id' => $id, 'id' => $taskId]);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Tarefa do projeto não encontrada.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao exibir a tarefa do projeto.']);
        }
    }

    /**
     *
     * @param type $id
     * @param type $taskId
     * @param Request $request
     * @return type
     */
    public function update($id, $taskId, Request $request) {
        try {
            return $this->service->update($request->all(), $taskId);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Tarefa não encontrada.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao atualizar a tarefa.']);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id, $taskId) {
        try {
            if ($this->repository->delete($taskId)) {
                return response(['msg' => 'Tarefa deletada com sucesso!']);
            } else {
                return response(['msg' => 'Erro ao deletar!']);
            }
            return $this->repository->find($taskId);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Tarefa não encontrada.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao deletar a tarefa.']);
        }
    }

}
