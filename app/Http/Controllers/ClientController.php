<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Repositories\ClientRepository;
use CodeProject\Services\ClientService;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    
    /**
     *
     * @var ClientRepository
     */
    private $repository;

    /**
     * @var ClientService
     */
    private $service;

    /**
     * ClientController constructor.
     * @param ClientRepository $repository
     * @param ClientService $service
     */
    public function __construct(ClientRepository $repository, ClientService $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * @param Request $request
     * @return static
     */
    public function store(Request $request)
    {
        try {
            return $this->service->create($request->all());
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao criar o cliente.']);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            return $this->repository->find($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Cliente não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao exibir o cliente.']);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($id, Request $request)
    {
        try {
            return $this->service->update($request->all(), $id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Cliente não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao atualizar o cliente.']);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        try {
            if ($this->repository->delete($id)) {
                return response(['msg' => 'Cliente deletado com sucesso!']);
            } else {
                return response(['error' => true, 'msg' => 'Erro ao deletar!']);
            }
            return $this->repository->find($id);
        } catch (\Illuminate\Database\QueryException $e) {
            return ['error'=>true, 'O cliente nao pode ser apagado pois existem projetos vinculados a ele.'];
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response(['error' => true, 'msg' => 'Cliente não encontrado.']);
        } catch (\Exception $e) {
            return response(['error' => true, 'msg' => 'Ocorreu algum erro ao deletar o cliente.']);
        }
    }
}
