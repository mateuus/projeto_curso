<?php

namespace CodeProject\Repositories;

use CodeProject\Entities\Client;
use \Prettus\Repository\Eloquent\BaseRepository;

class ClientRepositoryEloquent extends BaseRepository implements ClientRepository {

    /**
     * 
     * @return Client
     */
    public function model() {
        return Client::class;
    }

}
