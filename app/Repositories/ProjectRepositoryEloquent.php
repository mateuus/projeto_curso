<?php

namespace CodeProject\Repositories;

use CodeProject\Entities\Project;
use \Prettus\Repository\Eloquent\BaseRepository;

class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository {

    /**
     *
     * @return Project
     */
    public function model() {
        return Project::class;
    }

    /**
     * @param $projectId
     * @param $userId
     * @return bool
     */
    public function isOwner($projectId, $userId) {
        return $this->findWhere(['id' => $projectId, 'owner_id' => $userId])->count() > 0 ? true : false;
    }
}
