<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(CodeProject\Entities\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(CodeProject\Entities\Client::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'responsible' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'obs' => $faker->sentence
    ];
});

$factory->define(CodeProject\Entities\Project::class, function (Faker\Generator $faker) {
    return [
        'owner_id' => \CodeProject\Entities\User::all()->random()->id,
        'client_id' => \CodeProject\Entities\Client::all()->random()->id,
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'progress' => $faker->randomDigit,
        'status' => 1,
        'due_date' => $faker->dateTime
    ];
});

$factory->define(CodeProject\Entities\ProjectNote::class, function (Faker\Generator $faker) {
    return [
        'project_id' => \CodeProject\Entities\Project::all()->random()->id,
        'title' => $faker->word,
        'note' => $faker->paragraph,
    ];
});

$factory->define(CodeProject\Entities\ProjectTask::class, function (Faker\Generator $faker) {
    return [
        'project_id' => \CodeProject\Entities\Project::all()->random()->id,
        'name' => $faker->name,
        'status' => 1,
        'due_date' => $faker->dateTime,
        'start_date' => $faker->dateTime,
    ];
});

$factory->define(CodeProject\Entities\ProjectMembers::class, function (Faker\Generator $faker) {
    return [
        'project_id' => \CodeProject\Entities\Project::all()->random()->id,
        'user_id' => \CodeProject\Entities\User::all()->random()->id,
    ];
});