<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Seeder;

/**
 * Description of UsersTableSeeder
 *
 * @author mateus
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\CodeProject\Entities\User::class)->create([
            'name' => 'Mateus',
            'email' => 'mateuspastori@outlook.com',
            'password' => bcrypt(123456),
            'remember_token' => str_random(10),
        ]);
        factory(\CodeProject\Entities\User::class, 1)->create();
    }
    //put your code here
}
